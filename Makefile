IMG_BASE := registry.gitlab.com/euphon/docker-apps
IMG_TAG := $(shell git describe --always)
apps := \
	whisper

build:
	for app in $(apps); do \
		docker build -t $(IMG_BASE)/$$app:$(IMG_TAG) $$app; \
	done
